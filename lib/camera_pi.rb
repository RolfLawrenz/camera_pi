require 'byebug'
require 'andand'
require 'logger'
require 'active_support/inflector'

require "./lib/camera_pi/version"
require "./lib/camera_pi/operating_system"
require "./lib/camera_pi/camera"
require "./lib/camera_pi/camera_util"

# *********************** MAIN *********************************************
$LOG = Logger.new("logs/camera_pi_#{Time.now.strftime("%Y%m%d")}.log")
$LOG.datetime_format = '%Y-%m-%d %H:%M:%S'

$LOG.info("Camera Pi")
$LOG.info("---------")
start_time = Time.now
$LOG.info("#{start_time}")

$LOG.info("OS: #{CameraPi::OperatingSystem.type} - #{CameraPi::OperatingSystem.name}")

# Kill any conflicting processes
CameraPi::OperatingSystem.prepare_os

CameraUtil.show_programs

# Read camera type
camera = CameraPi::Camera.new
$LOG.info("Camera: #{camera.name}")

# If no camera, wait for up to a minute for user to plug in camera
CameraUtil.wait_for_camera_to_connect(camera)

# If running on Raspberry Pi (assume linux is on pi), set the system time based on camera
if CameraPi::OperatingSystem.linux?
  CameraUtil.set_system_time_from_camera(camera)
end

# Which program to run depends on settings on Camera
program = CameraUtil::detect_program(camera)

# Now run the program
program.run(camera) if program

# Close
camera.shutdown

end_time = Time.now
$LOG.info("\n#{end_time}")
$LOG.info("DONE. #{end_time - start_time} seconds")
