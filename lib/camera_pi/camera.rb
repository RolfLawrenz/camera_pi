require 'gphoto2'
require 'date'

module CameraPi
  class Camera

    attr_accessor :camera, :capture_count, :exp_comp_choices, :f_number_choices, :iso_choices

    FOCUS_MODES = {
        manual: "Manual",
        single: "AF-S",
        continuous: "AF-C",
    }

    SET_SETTINGS_RETRY_COUNT = 3
    SAVE_RETRY = 3

    def initialize
      @capture_count = 0
      begin
        @camera = GPhoto2::Camera.andand.first
        @exp_comp_choices = field_choices(['capturesettings','exposurecompensation'])
        @f_number_choices = field_choices(['capturesettings','f-number'])
        @iso_choices = field_choices(['imgsettings','iso'])
      rescue
        #  No Camera, try again later
      end
    end

    # This uses timezone in camera
    def datetime_as_long
      result = %x(gphoto2 --get-config datetime)
      fields = result.split
      index = fields.index('Current:')
      return nil unless index && index > 0
      fields[index + 1]
    end

    # This uses timezone in camera
    def datetime
      result = %x(gphoto2 --get-config datetime)
      fields = result.split("\n")
      fields.each do |field|
        if field.start_with?("Printable")
          date_str = field.partition(':')[2].strip
          return DateTime.strptime(date_str,"%a %b %d %H:%M:%S %Y")
        end
      end
      nil
    end

    # Uses UTC timezone
    def datetime_utc
      @camera['datetime'].value
    end

    def display_fields(widget, level = 0)
      indent = '  ' * level

      puts "#{indent}#{widget.name}"

      if widget.type == :window || widget.type == :section
        widget.children.each { |child| display_fields(child, level + 1) }
        return
      end

      indent << '  '

      puts "#{indent}type: #{widget.type}"
      puts "#{indent}value: #{widget.value}"

      case widget.type
        when :range
          range = widget.range
          step = (range.size > 1) ? range[1] - range[0] : 1.0
          puts "#{indent}options: #{range.first}..#{range.last}:step(#{step})"
        when :radio, :menu
          puts "#{indent}options: #{widget.choices.inspect}"
      end
    end

    def exposure_compensation
      @camera['exposurecompensation'].value
    end

    def increase_field_value(field_name, values)
      field_value = @camera[field_name].value
      index = values.index(field_value)
      $LOG.info("increase #{field_name} (#{field_value} to #{values[index+1]})")
      @camera[field_name] = values[index+1]
      save_retry
    end

    def decrease_field_value(field_name, values)
      field_value = @camera[field_name].value
      index = values.index(field_value)
      $LOG.info("decrease #{field_name} (#{field_value} to #{values[index-1]})")
      @camera[field_name] = values[index-1]
      save_retry
    end

    def save_retry
      count = 0
      while count < SAVE_RETRY do
        begin
          @camera.save
          break
        rescue
          count += 1
          $LOG.info("ERROR Saving - retry #{count}")
        end
      end
    end

    def exposure_program
      @camera['expprogram'].value
    end

    # Value meanings: https://en.wikipedia.org/wiki/Exposure_value
    # 12 - 16  - Daylight
    # 12       - Sunrise / Sunset
    # 5 - 11   - Dusk / Dawn
    # 7 - 8    - Night, bright street
    # 5 - 8    - Inside buildings, artificial light
    # 5        - Night, near lighted buildings
    # 2 - 3    - Night, distant lighted buildings
    # -2       - Night, dark landscape at full moon
    # -4       - Night, dark landscape at half moon
    # -6 - -8  - Night, dark landscape at new moon
    # -3 - -9  - Night, stars
    # -10      - Night, weak celestial bodies, nebulae
    # *** Takes into account exposure compensation
    def ev
      reload
      _iso = iso.to_f
      _shutterspeed = shutterspeed_value
      _f_number = f_number_value.to_f
      _exp_comp = exposure_compensation.to_f

      # EV = log2(100 x f-number^2 / (ISO x shutter))
      Math::log2(100 * (_f_number ** 2) / (_iso * _shutterspeed)) + _exp_comp
    end

    def name
      @camera['cameramodel'].value
    end

    def reload
      @camera.reload
    end

    def shutterspeed
      @camera['shutterspeed2'].value
    end

    def shutterspeed_value
      values = shutterspeed.split("/")
      values.count == 2 ? values[0].to_f / values[1].to_f : values[0].to_f
    end

    def f_number
      @camera['f-number'].value
    end

    def f_number_value
      f_number.split('/')[1]
    end

    def iso
      @camera['iso'].value
    end

    def iso_choice_from_ev
      _ev = ev
      if _ev > 12
        "200"
      elsif _ev > 10
        "400"
      elsif _ev > 7
        "800"
      elsif _ev > 5
        "1000"
      elsif _ev > 3
        "1250"
      else
        "1600"
      end
    end

    def capture_photo
      $LOG.info("##{@capture_count} Capture Photo")
      puts("##{@capture_count} Capture Photo")
      @capture_count += 1
      @camera.capture
    end

    def capture_photo_and_download
      capture_photo.save
    end

    def shutdown
      @camera.finalize
    end

    # WARNING: This will open shutter but not close, like for a LV Movie
    def preview
      @camera.preview
    end

    def set_settings(settings)
      return if settings.nil?

      count = 0
      while count < SET_SETTINGS_RETRY_COUNT do
        $LOG.info("set_settings ##{count}")
        begin
          if settings.is_a?(Hash)
            set_settings_hash(settings)
          elsif settings.is_a?(Array)
            settings.each do |stgs|
              set_settings_hash(stgs)
              # Need to wait at least 3 seconds here for settings to take effect
              sleep(4)
            end
          end
          break
        rescue
          count += 1
          $LOG.info("set settings FAILED - retry")
          raise "SETTINGS ERROR" if count == SET_SETTINGS_RETRY_COUNT
        end
      end
    end

    def field_choices(field_tree)
      widget = @camera.window
      field_tree.each do |field|
        widget = widget.children.select{|c| c.name == field }[0]
      end
      widget.choices
    end

    private

    def set_settings_hash(settings)
      settings.each do |setting, value|
        $LOG.info("  #{setting}: #{value}")
        @camera[setting] = value
      end
      $LOG.info("save settings")
      @camera.save
    end

  end
end
