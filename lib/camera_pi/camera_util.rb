require "./lib/camera_pi/program/timelapse_program"

module CameraUtil
  NUMBER_OF_MINUTES_TO_WAIT_BEFORE_EXIT = 1
  WAIT_POLL_INTERVAL = 5

  # Program to run is set my initial settings on camera
  PROGRAM_FROM_CAMERA_SETTINGS = {
      timelapse: {
          'whitebalance' => 'Cloudy'
      }
  }
  DEFAULT_PROGRAM = :timelapse

  PROGRAM_DESC = {
      timelapse: "Set WhiteBalance to 'Cloudy', shutter speed determines delay, less that 1 second is "\
                 "inverted, eg 1/10 is 10 seconds. Change Exposure mode to stop. Set focus and change "\
                 "focus to manual."
  }

# -----------------------METHODS ----------------------------------------
  def CameraUtil.wait_for_camera_to_connect(camera)
    if !camera.camera
      $LOG.info("No Camera!")
      count = NUMBER_OF_MINUTES_TO_WAIT_BEFORE_EXIT * 60 / WAIT_POLL_INTERVAL  # Wait for 4 minutes, if no camera, abort
      while count > 0
        sleep (5)
        camera = CameraPi::Camera.new
        break if camera.camera
        print "."
        count -= 1
      end
      unless camera.camera
        $LOG.info("No Camera. Exiting.")
        exit
      end
    end
  end

  def CameraUtil.set_system_time_from_camera(camera)
    $LOG.info("Setting system time..")
    datetime = camera.datetime_as_long
    $LOG.info("Camera datetime (long):\n#{datetime}")
    system "date -s #{datetime}" if datetime
  end

  # Depending on initial settings on the camera, select the program to run
  def CameraUtil.detect_program(camera)
    PROGRAM_FROM_CAMERA_SETTINGS.each do |program_name, program_settings|
      match = true
      program_settings.each do |setting_name, setting_value|
        unless camera.camera[setting_name].value == setting_value
          match = false
          break
        end
      end
      return create_program(program_name) if match
    end
    $LOG.info("No matching program, using default program: #{DEFAULT_PROGRAM}")
    return create_program(DEFAULT_PROGRAM)
  end

  def CameraUtil.create_program(program_name)
    return nil unless program_name
    return "CameraPi::Program::#{program_name.to_s.camelize}Program".constantize.new(program_name)
  end

# -----------------------END METHODS----------------------------------------

# -----------------------PROGRAMS----------------------------------------
  def CameraUtil.show_programs
    puts("\nPROGRAMS:")
    puts("Set camera settings to select program:")
    PROGRAM_FROM_CAMERA_SETTINGS.each do |program_name, program_settings|
      puts("- #{program_name}")
      puts("  DESC: #{PROGRAM_DESC[program_name]}")
      program_settings.each do |setting_name, setting_value|
        puts("  TRIGGER: #{setting_name} = #{setting_value}")
      end
    end
    puts("")
  end
# -----------------------END PROGRAMS----------------------------------------

end
