module CameraPi
  module Program
    class BaseProgram
      attr_accessor :name, :day_stage

      FINAL_EXP_COMP = {
          night_to_day: 0,
          day_to_night: -2,
      }
      FINAL_F_NUMBER = {
          night_to_day: "f/9",
          day_to_night: "f/2.8",
      }

      # Change exposure gradually, after every n captures until final reached
      EXP_COMP_COUNT_CHANGE = 3
      F_NUMBER_COUNT_CHANGE = 3

      NIGHT_EV_CHANGE = 5
      DUSK_EV_CHANGE = 10

      def initialize(name)
        @name = name
      end

      # Before taking any shots, change camera settings
      def prepare_camera(camera)
        $LOG.info("Prepare Camera")

        camera.set_settings(initial_program_camera_settings)

        adjust_camera_settings_from_ev(camera)
        # Initial settings for night or day
        ev = camera.ev
        set_camera_day_settings(camera, ev)
      end

      def set_camera_day_settings(camera, ev)
        if ev < NIGHT_EV_CHANGE
          camera.set_settings(night_camera_settings)
        elsif ev < DUSK_EV_CHANGE
          camera.set_settings(dusk_camera_settings)
        else
          camera.set_settings(day_camera_settings)
        end
      end

      def adjust_camera_settings_from_ev(camera)
        ev = camera.ev
        $LOG.info("adjust_camera_settings_from_ev iso:#{camera.iso} shutter:#{camera.shutterspeed} fn:#{camera.f_number} ev=#{ev}")
        set_camera_settings_from_ev(camera, ev)
      end

      def set_camera_settings_from_ev(camera, ev)
        hour_of_day = Time.now.hour

        # Hour of day to work out if we are going from night to day or day to night
        day_stage = hour_of_day < 12 ? :night_to_day : :day_to_night

        set_camera_day_settings(camera, ev) if ev == NIGHT_EV_CHANGE || ev == DUSK_EV_CHANGE

        # Will make dawn/dusk a time to change from day to night settings.
        # Exposure compensation will change
        if ev.between?(8,10)
          $LOG.info("EV CHANGE DUSK/DAWN")
          final_exp_comp = FINAL_EXP_COMP[day_stage]

          puts "hour=#{hour_of_day} day_stage=#{day_stage}"

          if final_exp_comp.to_s != camera.exposure_compensation.to_s
            # Only change every n capture
            if camera.capture_count % EXP_COMP_COUNT_CHANGE == 0
              day_stage == :night_to_day ? camera.increase_field_value("exposurecompensation", camera.exp_comp_choices) :
                                           camera.decrease_field_value("exposurecompensation", camera.exp_comp_choices)
            end
          end
        end

        # Will change f number for day/night
        if ev.between?(0,3)
          $LOG.info("EV CHANGE AT NIGHT")
          final_f_number = FINAL_F_NUMBER[day_stage]

          if final_f_number.to_s != camera.f_number.to_s
            # Only change every n capture
            if camera.capture_count % F_NUMBER_COUNT_CHANGE == 0
              day_stage == :night_to_day ? camera.increase_field_value("f-number", camera.f_number_choices) :
                                           camera.decrease_field_value("f-number", camera.f_number_choices)
            end
          end
        end

        # Set ISO value based on EV value
        current_iso = camera.iso
        new_iso = camera.iso_choice_from_ev
        if new_iso != current_iso
          camera.set_settings({ "iso" => new_iso })
        end

      end

      def run(camera)
        raise NotImplementedError
      end
    end
  end
end
