require './lib/camera_pi/program/base_program'

module CameraPi
  module Program
    class TimelapseProgram < CameraPi::Program::BaseProgram

      # These settings are used to capture initial image to get a sense of lighting
      INITIAL_TIMELAPSE_CAMERA_SETTINGS = [
        #  These settings must be set first
        {
          "expprogram" => "A",
        },
        #  Then these
        {
          "imagesize" => "2144x1424",
          "whitebalance" => "Cloudy",
          "iso" => "400",
          #"autoiso" => "On",
          #"isoautohilimit" => "1600",
          "highisonr" => "Off",
          "longexpnr" => "Off",
          "autofocusmode2" => "On",
          "assistlight" => "Off",
          "exposurecompensation" => "0",
          "af-area-illumination" => "Off",
          "f-number" => "#{CameraPi::Program::BaseProgram::FINAL_F_NUMBER[:night_to_day]}",
          "imagequality" => "JPEG Basic",
          "focusmode" => "AF-S",
          "capturemode" => "Single Shot",
          "exposuremetermode" => "Multi Spot",
          "capturetarget" => "Internal RAM"  # When photo taken, just keep in RAM for first one
        },
      ]

      COMMON_CAPTURE_TIMELAPSE_CAMERA_SETTINGS = {
          "imagesize" => "4288x2848",
          "imagequality" => "JPEG Fine",
          "capturetarget" => "Memory card",
          "autofocusmode2" => "Off",
      }

      DAY_CAPTURE_TIMELAPSE_CAMERA_SETTINGS = COMMON_CAPTURE_TIMELAPSE_CAMERA_SETTINGS.merge({
          "highisonr" => "Off",
          "longexpnr" => "Off",
          "iso" => "200",
      })

      DUSK_CAPTURE_TIMELAPSE_CAMERA_SETTINGS = COMMON_CAPTURE_TIMELAPSE_CAMERA_SETTINGS.merge({
          "highisonr" => "Off",
          "longexpnr" => "Off",
          "iso" => "800",
      })

      NIGHT_CAPTURE_TIMELAPSE_CAMERA_SETTINGS = COMMON_CAPTURE_TIMELAPSE_CAMERA_SETTINGS.merge({
          "highisonr" => "Normal",
          "longexpnr" => "On",
          "iso" => "1600",
      })


      def initialize(name)
        super(name)
      end

      def run(camera)
        $LOG.info("RUN #{@name} PROGRAM")

        interval = read_camera_interval(camera)
        $LOG.info("Interval: #{interval} seconds")

        prepare_camera(camera)

        done = false
        count  = 0
        while !done
          camera.capture_photo
          adjust_camera_settings_from_ev(camera)

          sleep(interval.to_i)

          count += 1
        end
      end

      def read_camera_interval(camera)
        interval = camera.shutterspeed
        if interval.include?('/') && interval.index("/") > 0
          interval = (interval.partition("/")[2].to_f / interval.partition("/")[0].to_f).to_i
        end
        interval
      end

      def initial_program_camera_settings
        INITIAL_TIMELAPSE_CAMERA_SETTINGS
      end

      def night_camera_settings
        NIGHT_CAPTURE_TIMELAPSE_CAMERA_SETTINGS
      end

      def dusk_camera_settings
        DUSK_CAPTURE_TIMELAPSE_CAMERA_SETTINGS
      end

      def day_camera_settings
        DAY_CAPTURE_TIMELAPSE_CAMERA_SETTINGS
      end

    end
  end
end
