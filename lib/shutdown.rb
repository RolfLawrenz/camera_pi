# Look for a pin change to then shutdown the pi
require 'pi_piper'
include PiPiper

puts "Starting Shutdown Script"
SHUTDOWN_PIN = 27
def shutdown_pi
  puts "Shutting down..."
  exec 'sudo shutdown -h now'
end

count = 0
watch :pin => SHUTDOWN_PIN, :direction => :in, :pull => :down do |pin|
  count += 1
  if count == 1
    shutdown_pi
  end
end

PiPiper.wait
